﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OpenWeatherMicro5.ServiceClient;

namespace OpenWeatherMicro5.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private readonly ILogger<WeatherForecastController> _logger;
        private readonly OpenWeatherClient _openWeatherClient;

        public WeatherForecastController(ILogger<WeatherForecastController> logger, OpenWeatherClient openWeatherClient)
        {
            _logger = logger;
            _openWeatherClient = openWeatherClient;
        }

        [HttpGet]
        [Route("{city}")]
        public async Task<WeatherForecast> GetCityWeather(string city)
        {
            
                var forecast = await _openWeatherClient.GetCurrentWeatherAsync(city);
                return new WeatherForecast
                {
                    Date = DateTimeOffset.FromUnixTimeSeconds(forecast.dt).DateTime,
                    Main = forecast.weather[0].main,
                    Description = forecast.weather[0].description,
                    TemperatureC = (int)forecast.main.temp,
                    TemperatureFeelsLike = (int)forecast.main.feels_like,
                    TempMin = (int)forecast.main.temp_min,
                    TempMax = (int)forecast.main.temp_max,
                    Pressure = forecast.main.pressure,
                    Humidity = forecast.main.humidity,
                    WindSpeed = (int)forecast.wind.speed,
                    Name = forecast.name
                }; 
        }
    }
}
