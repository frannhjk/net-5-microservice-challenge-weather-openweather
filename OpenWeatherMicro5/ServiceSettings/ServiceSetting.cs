namespace OpenWeatherMicro5.ServiceSettings
{
    public class ServiceSetting
    {
        public string OpenWeatherBaseUrl {get; set;}
        public string ApiKey { get; set; }
    }
}