using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using OpenWeatherMicro5.ServiceSettings;

namespace OpenWeatherMicro5.ServiceClient
{
    public class OpenWeatherClient
    {
        private readonly HttpClient _httpClient;
        private readonly ServiceSetting _settings;
 
        public OpenWeatherClient(HttpClient httpClient, IOptions<ServiceSetting> settings)
        {
            _httpClient = httpClient;
            _settings = settings.Value;
        }

        // Feature: Records - Para representar el objeto completo obtenido en el response del request
        public record Weather(string main, string description);
        public record Main(decimal temp, decimal feels_like, decimal temp_min, decimal temp_max, int pressure, int humidity);
        public record Wind(decimal speed);
        public record Forecast (Weather[] weather, Main main, Wind wind, long dt, string name);

        public async Task<Forecast> GetCurrentWeatherAsync(string city)
        {
            var forecast = await _httpClient.GetFromJsonAsync<Forecast>($"https://{_settings.OpenWeatherBaseUrl}/data/2.5/weather?q={city}&appid={_settings.ApiKey}");
            return forecast;
        }

        
    } 
}